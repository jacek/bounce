#ifndef Particle_hh
#define Particle_hh

extern const int minColumn;
extern const int maxColumn;

class Screen;

class Particle {
protected:
  char symbol;
  double position;
  double speed;
public:
  Particle(const char sym, const double v, const double pos)
    : symbol(sym), position(pos), speed(v) { }
  Particle& operator=(const Particle& rhs);
  void draw(Screen& screen) const;
  virtual void move();
  virtual ~Particle() {}
};

class MagicParticle : public Particle {
public:
  MagicParticle(const char sym, const double pos, const double v)
    : Particle(sym, pos,v) { }
 virtual void move();
};

#endif
