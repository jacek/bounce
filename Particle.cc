#include "Particle.hh"
#include "Screen.hh"

extern const int maxColumn = 80;
extern const int minColumn =  0;

Particle& Particle::operator=(const Particle& rhs) {
  this->symbol   = rhs.symbol;
  this->position = rhs.position;
  this->speed    = rhs.speed;
  return *this;
}

void Particle::draw(Screen& screen) const {
  screen[position] = symbol;
}

void Particle::move() {
  position += speed;
  if (position >= maxColumn) {
    position = maxColumn;
    speed = -speed;
  } else if (position < minColumn) {
    position = minColumn;
    speed = -speed;
  }
}

void MagicParticle::move() {
  position += speed;
  if (position >= maxColumn) {
    position = minColumn;
  } else if (position < minColumn) {
    position = maxColumn;
  }
}
