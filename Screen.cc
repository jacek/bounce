#include <iostream>
#include "Screen.hh"

using std::cout; using std::endl;


Screen::Screen(size_t size) : size(size), buffer(new char[size]) { }

void Screen::flush() {
  for(size_t i=0; i<size; ++i) {
    cout << buffer[i];
  }
  cout << endl;
}

Screen::Screen(const Screen& other) : size(other.size), buffer(new char[other.size]) {
  std::copy(other.buffer, other.buffer+size, this->buffer);
}

Screen& Screen::operator=(Screen copy_of_other) {
  std::swap(this->buffer, copy_of_other.buffer);
  std::swap(this->size  , copy_of_other.size);
  return *this;
}

Screen::~Screen() {
  delete [] buffer;
}

char& Screen::operator[] (unsigned pos) {
  return this->buffer[pos];
}

void Screen::clear() {
  for(size_t i=0; i<size; ++i) {
    buffer[i] = ' ';
  }
}
