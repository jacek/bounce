#ifndef Screen_hh
#define Screen_hh

#include <cstddef>

class Screen {

private:
  size_t size;
  char* buffer;

public:
  Screen(size_t size);
  Screen(const Screen& other);
  Screen& operator=(Screen copy_of_other);
  ~Screen();
  char& operator[] (unsigned pos);
  void flush();
  void clear();
};

#endif
