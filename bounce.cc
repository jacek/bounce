#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>

#include "Screen.hh"
#include "Particle.hh"

const int screenSize = maxColumn + 1;

int main() {

  std::ifstream in("particles.cfg");
  if (!in) {
    std::cerr << "Could not open 'particles.cfg'" << std::endl;
    return EXIT_FAILURE;
  }

  unsigned numberOfParticles;
  in >> numberOfParticles;

  std::vector<Particle*> particle;

  for(unsigned i=0; i<numberOfParticles; ++i) {
    char kind, s;
    double p, v;
    in >> kind >> s >> v >> p;
    if (kind == 'm')
      particle.push_back(new MagicParticle(s, v, p));
    else
      particle.push_back(new      Particle(s, v, p));
  }

  int timeStep = 0;
  const int stopTime = 60;

  Screen screen(screenSize);

  while (timeStep < stopTime) {
    screen.clear();
    for (unsigned i=0; i<numberOfParticles; ++i) {
      particle[i]->draw(screen);
      particle[i]->move();
    }
    screen.flush();
    timeStep++;
  }
  for (unsigned i=0; i<particle.size(); ++i)
    delete particle[i];
}
